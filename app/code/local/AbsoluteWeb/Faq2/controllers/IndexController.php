<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq2_IndexController extends Mage_Core_Controller_Front_Action
{

    private function _initLayout()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * This method is output all questions and answers to them
     * For example you may visit the following URL http://example.com/frontName/index/getAllFaq
     */
    public function getAllFaqAction()
    {
        $this->_initLayout();
    }

    /**
     * Render form to add new faq
     */
    public function addNewFaqAction()
    {
        // the same form is used to create and edit
        $model = Mage::getModel('absoluteweb_faq2/faq');
        Mage::register('aws_faq_model', $model);
        $this->_initLayout();
    }

    /**
     * Render form to edit faq
     * @throws Mage_Core_Exception
     */
    public function editFaqByIdAction()
    {
        // @todo here you must load and render layout
        $id = $this->getRequest()->getParam('id', null);

        if(!$id){
            $this->_initLayout();
            return ;
        }

        $model = Mage::getModel('absoluteweb_faq2/faq')
            ->load($id);

        if(!$model->getId()) {
            $this->_initLayout();
            return ;
        }
        Mage::register('aws_faq_model', $model);
        $this->_initLayout();
    }

    /**
     * Save faq by using id or add new record
     */
    public function saveAction()
    {
        /**
         * @todo here you must realize create or edit logic
         *      if id exist in post, you can load model by using it otherwise create new record
         *      add message about successful saving or editing by using session (see models such as Mage_Core_Model_Session and extended classes)
         */
        $faqObject = Mage::getModel('absoluteweb_faq2/faq');
        $id = $this->getRequest()->getParam('id_enter', null);

        if ((!intval($id)) && ($id!=null)){
            $this->_setErrorMessage('invalid id');
            $this->_redirect('zhukov/index/getAllFaq');
            return;
        }
        $data = array(
            'question' => $this->getRequest()->getParam('question_enter', ''),
            'answer' => $this->getRequest()->getParam('answer_enter', ''),
            'date' => date('y-m-d h:i:s')
        );

        if($id){
            $faqObject = $faqObject->load($id);
        }

        $faqObject->addData($data)->save();

        $viewMessage = $this->__('message vas change');

        if($faqObject->isObjectNew()){

            /** @var Mage_Core_Model_Session $session */
            $viewMessage = $this-> __('message vas added');
        }
        $this->_setSuccessMessage($viewMessage);
        $this->_redirect('zhukov/index/getAllFaq');

    }

    /**
     * Delete faq by id
     * @throws Exception
     */
    public function deleteAction()
    {
        /**
         * @todo get id sent by url and delete faq
         *       add message by using session
         */
        $id = $this->getRequest()->getParam('id');
        $faqObject = Mage::getModel('absoluteweb_faq2/faq');
        $faqObject->load($id)->delete();
        $this->_redirect('zhukov/index/getAllFaq');
    }

    private function _setSuccessMessage($viewMessage)
    {
        return Mage::getSingleton('core/session')->addSuccess($this->__($viewMessage));
    }

    private function _setErrorMessage($viewMessage)
    {
        return Mage::getSingleton('core/session')->addError($this->__($viewMessage));
    }

}
