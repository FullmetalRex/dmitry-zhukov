<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright :copyright: 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/new', array('id' => $row->getEntityId()));
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setDefaultSort('id');
        $this->setId('absoluteweb_news_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     *
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('absoluteweb_news/news')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Configuration of grid
     *
     */
    protected function _prepareColumns()
    {

        $this->addColumn('entity_id',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('Id'),
                'type' => 'range',
                'align' =>'right',
                'width' => '50px',
                'index' => 'entity_id',
                'sortable' => true
            )
        );
        $this->addColumn('title',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('title'),
                'index' => 'title',
                'type' => 'text',
                'sortable' => true
            )
        );
        $this->addColumn('content',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('content'),
                'index' => 'content',
                'type' => 'text',
                'renderer' => 'AbsoluteWeb_News_Block_Adminhtml_News_Grid_Column_Renderer_Striptagshtml',
                'string_limit' => '255',
                'sortable' => true
            )
        );
        $this->addColumn('create_date',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('create date'),
                'index' => 'create_date',
                'type' => 'datetime',
                'sortable' => true
            )
        );
        $this->addColumn('update_date',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('update date'),
                'index' => 'update_date',
                'type' => 'datetime',
                'sortable' => true
            )
        );
        $this->addColumn('publish',
            array(
                'header'=> Mage::helper('absoluteweb_news')->__('publish'),
                'index' => 'publish',
                'type' => 'options',
                'options' => array('0' => 'No', '1' => 'Yes'),
                'sortable' => true
            )
        );
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('deleteMass', array(
            'label' => Mage::helper('absoluteweb_news')->__('Delete'),
            'url' => $this->getUrl('*/*/deleteMass'),
            'confirm' => Mage::helper('absoluteweb_news')->__('Are you sure about that?')
        ));
        return $this;
    }

}