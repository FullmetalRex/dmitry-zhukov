<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_News_Block_Adminhtml_News_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Prepare form for render
     */
    protected $_blockGroup = 'absoluteweb_news/news';

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $news  = $this->_getModel();
        $helper = Mage::helper('absoluteweb_news');
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post'
        ));

        $form->setUseContainer(true);
        $this->setForm($form);
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $helper->__('Create new news')));

        if ($news->getId()) {
            $fieldset->addField('entity_id', 'hidden', array(
                'label' => 'id',
                'name' => 'entity_id',
                'value' => $news->getId()
            ));
        }
        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('absoluteweb_news')->__('Title'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title'
        ));
        $fieldset->addField('image', 'text', array(
            'label' => Mage::helper('absoluteweb_news')->__('Image'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'image'
        ));
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config');
        $fieldset->addField('content', 'editor', array(
            'label' => Mage::helper('absoluteweb_news')->__('Content'),
            'class' => 'required-entry',
            'wysiwyg' => true,
            'required' => true,
            'config' => $wysiwygConfig,
            'name' => 'content'
        ));
        $fieldset->addField('publish', 'select', array(
            'values' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray('yes', 'no'),
            'label' => Mage::helper('absoluteweb_news')->__('publish'),
            'class' => 'required-entry',
            'required' => true,
            'name' => 'publish'
        ));
        $form->addValues($news->getData());
    }

    private function _getModel()
    {
        $model = Mage::registry('current_news');
        if (!$model || !($model instanceof AbsoluteWeb_News_Model_News)) {
            Mage::throwException('Model is not good');
        }
        return $model;
    }
}